import 'dart:async';

import 'package:flutter/material.dart';

class LoadingAvatar extends StatefulWidget {
  @override
  _LoadingAvatarState createState() => _LoadingAvatarState();
}

class _LoadingAvatarState extends State<LoadingAvatar> {
  bool blinking = false;
  StreamSubscription<int> sub;

  @override
  void initState() {
    super.initState();
    sub = Stream.periodic(const Duration(milliseconds: 500), (i) => i)
        .listen((_) => setState(() {
              blinking = !blinking;
            }));
  }

  @override
  void deactivate() {
    super.deactivate();
    sub?.cancel();
    sub = null;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: blinking ? 1 : 0.4,
      curve: blinking ? Curves.easeInSine : Curves.easeOutSine,
      duration: Duration(milliseconds: 300),
      child: CircleAvatar(),
    );
  }
}
