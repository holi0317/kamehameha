import 'package:flutter/material.dart';
import 'package:kamehameha/engraving_page.dart';

/// Accept raw gcode from text field then sent them to engraver.
class ManualPage extends StatefulWidget {
  @override
  _ManualPageState createState() => _ManualPageState();
}

class _ManualPageState extends State<ManualPage> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Raw gcode"),
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        left: false,
        right: false,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: TextField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Raw G-Code to be sent to engraver",
                    labelText: 'Code',
                  ),
                  controller: controller,
                  keyboardType: TextInputType.multiline,
                  textCapitalization: TextCapitalization.none,
                  autocorrect: false,
                  autofocus: true,
                  // TODO Set maxLines to null and expands to true when flutter updates
                  maxLines: 10,
                ),
              ),
              Center(
                child: RaisedButton(
                  onPressed: _submit,
                  child: const Text("Submit"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => EngravePage(gcode: controller.text)),
    );
  }
}
