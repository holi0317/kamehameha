import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:kamehameha/crop_page/convert_image.dart';
import 'package:kamehameha/crop_page/crop_page.dart';
import 'package:kamehameha/draw_page/color_dialog.dart';
import 'package:kamehameha/draw_page/painter.dart';
import 'package:kamehameha/draw_page/width_dialog.dart';
import 'package:kamehameha/engraving_page.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';

class DrawCanvas extends StatefulWidget {
  @override
  DrawCanvasState createState() => new DrawCanvasState();
}

class DrawCanvasState extends State<DrawCanvas> with TickerProviderStateMixin {
  AnimationController controller;
  List<Offset> points = <Offset>[];
  Color color = Colors.black;
  StrokeCap strokeCap = StrokeCap.round;
  double strokeWidth = 5.0;
  List<Picture> layers = <Picture>[];
  bool willChange = false;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: GestureDetector(
          onPanStart: (DragStartDetails details) {
            if (!controller.isDismissed) {
              controller.reverse();
            }
            setState(() {
              willChange = true;
            });
          },
          onPanUpdate: (DragUpdateDetails details) {
            RenderBox object = context.findRenderObject();
            Offset localPosition = object.globalToLocal(details.globalPosition);
            if (points.isEmpty ||
                (localPosition - points.last).distanceSquared > 5) {
              setState(() {
                points = List.from(points);
                points.add(localPosition);
              });
            }
          },
          onPanEnd: (DragEndDetails details) {
            setState(() {
              willChange = false;
            });
            _saveLayer();
          },
          child: RepaintBoundary(
            child: CustomPaint(
              painter: Painter(
                  points: points,
                  color: color,
                  strokeCap: strokeCap,
                  strokeWidth: strokeWidth,
                  layers: layers),
              size: Size.infinite,
              isComplex: true,
              willChange: willChange,
            ),
          ),
        ),
      ),
      floatingActionButton:
          Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Container(
            height: 70.0,
            width: 56.0,
            alignment: FractionalOffset.topCenter,
            child: ScaleTransition(
              scale: CurvedAnimation(
                parent: controller,
                curve: Interval(0.0, 1.0 - 0 / 3 / 2.0, curve: Curves.easeOut),
              ),
              child: FloatingActionButton(
                heroTag: 'undoBtn',
                mini: true,
                child: Icon(Icons.undo),
                onPressed: _undo,
              ),
            )),
        Container(
          height: 70.0,
          width: 56.0,
          alignment: FractionalOffset.topCenter,
          child: ScaleTransition(
            scale: CurvedAnimation(
              parent: controller,
              curve: Interval(0.0, 1.0 - 0 / 3 / 2.0, curve: Curves.easeOut),
            ),
            child: FloatingActionButton(
              heroTag: 'painterBtn',
              mini: true,
              child: Icon(Icons.delete),
              onPressed: () {
                setState(() {
                  points = [];
                  layers = [];
                });
              },
            ),
          ),
        ),
        Container(
          height: 70.0,
          width: 56.0,
          alignment: FractionalOffset.topCenter,
          child: ScaleTransition(
            scale: CurvedAnimation(
              parent: controller,
              curve: Interval(0.0, 1.0 - 1 / 3 / 2.0, curve: Curves.easeOut),
            ),
            child: FloatingActionButton(
              heroTag: 'screenshotBtn',
              mini: true,
              child: Icon(Icons.save),
              onPressed: () => _save(context.size),
            ),
          ),
        ),
        Container(
          height: 70.0,
          width: 56.0,
          alignment: FractionalOffset.topCenter,
          child: ScaleTransition(
            scale: CurvedAnimation(
              parent: controller,
              curve: Interval(0.0, 1.0 - 1 / 3 / 2.0, curve: Curves.easeOut),
            ),
            child: FloatingActionButton(
              heroTag: 'widthBtn',
              mini: true,
              child: Icon(Icons.lens),
              onPressed: () async {
                double temp;
                temp = await showDialog(
                    context: context,
                    builder: (context) =>
                        WidthDialog(strokeWidth: strokeWidth));
                if (temp != null) {
                  _saveLayer();
                  setState(() {
                    strokeWidth = temp;
                  });
                }
              },
            ),
          ),
        ),
        Container(
            height: 70.0,
            width: 56.0,
            alignment: FractionalOffset.topCenter,
            child: ScaleTransition(
                scale: CurvedAnimation(
                  parent: controller,
                  curve:
                      Interval(0.0, 1.0 - 2 / 3 / 2.0, curve: Curves.easeOut),
                ),
                child: FloatingActionButton(
                    heroTag: 'colorBtn',
                    mini: true,
                    child: Icon(Icons.color_lens),
                    onPressed: () async {
                      Color temp;
                      temp = await showDialog(
                          context: context,
                          builder: (context) => ColorDialog());
                      if (temp != null) {
                        _saveLayer();
                        setState(() {
                          color = temp;
                        });
                      }
                    }))),
        FloatingActionButton(
          heroTag: 'returnBtn',
          child: AnimatedBuilder(
            animation: controller,
            builder: (BuildContext context, Widget child) {
              return Transform(
                transform: Matrix4.rotationZ(controller.value * 0.5 * math.pi),
                alignment: FractionalOffset.center,
                child: Icon(Icons.brush),
              );
            },
          ),
          onPressed: () {
            if (controller.isDismissed) {
              controller.forward();
            } else {
              controller.reverse();
            }
          },
        ),
      ]),
    );
  }

  void _saveLayer() {
    setState(() {
      final recorder = PictureRecorder();
      final canvas = Canvas(recorder);

      paintPoints(
          canvas: canvas,
          color: color,
          points: points,
          strokeCap: strokeCap,
          strokeWidth: strokeWidth);

      points.clear();
      final layer = recorder.endRecording();
      layers.add(layer);
    });
  }

  void _undo() {
    setState(() {
      if (layers.isNotEmpty) {
        final clone = List.of(layers);
        clone.removeLast();
        layers = clone;
      }
    });
  }

  Future<void> _save(Size size) async {
    final recorder = PictureRecorder();
    final canvas = Canvas(recorder);

    // Draw a background color
    final paint = Paint();
    paint.color = Color.fromARGB(255, 255, 255, 255);
    canvas.drawRect(Rect.fromLTRB(0, 0, size.width, size.height), paint);

    for (var layer in layers) {
      canvas.drawPicture(layer);
    }

    // Assume: User is not drawing when saving
    // So no need to care [points] variable.
    final picture = recorder.endRecording();
    final image =
        await picture.toImage(size.width.round(), size.height.round());
    final byteData = await image.toByteData(format: ImageByteFormat.png);
    final memImage = Uint8List.view(byteData.buffer);

    final tempDir = await getTemporaryDirectory();
    final uuid = Uuid();
    final file = File(p.join(tempDir.path, "${uuid.v4()}.png"));
    await file.writeAsBytes(memImage.toList(growable: false));

    ConvertRes code = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CropPage(image: file)),
    );

    if (code == null) {
      return;
    }

    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => EngravePage(
                gcode: code.gcode,
                image: MemoryImage(code.image),
              )),
    );
  }
}
