import 'dart:ui';

import 'package:flutter/material.dart';

/// Paint points on one layer to the given canvas
void paintPoints(
    {Canvas canvas,
    Color color,
    List<Offset> points,
    StrokeCap strokeCap,
    double strokeWidth}) {
  final paint = Paint();
  paint.color = color;
  paint.strokeCap = strokeCap;
  paint.strokeWidth = strokeWidth;
  for (int i = 0; i < points.length - 1; i++) {
    if (points[i] != null && points[i + 1] != null) {
      canvas.drawLine(points[i], points[i + 1], paint);
    }
  }
}

class Painter extends CustomPainter {
  List<Offset> points;
  Color color;
  StrokeCap strokeCap;
  double strokeWidth;
  List<Picture> layers;

  Painter(
      {this.points,
      this.color,
      this.strokeCap,
      this.strokeWidth,
      this.layers = const []});

  @override
  void paint(Canvas canvas, Size size) {
    for (var layer in layers) {
      canvas.drawPicture(layer);
    }

    paintPoints(
        canvas: canvas,
        color: color,
        points: points,
        strokeCap: strokeCap,
        strokeWidth: strokeWidth);
  }

  @override
  bool shouldRepaint(Painter oldPainter) =>
      oldPainter.points != points || oldPainter.layers != layers;
}
