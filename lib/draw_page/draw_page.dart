import 'package:flutter/material.dart';
import 'package:kamehameha/draw_page/draw_canvas.dart';

class DrawPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Draw!"),
      ),
      // The body is in it's own widget so that it's width and height is the
      // Whole canvas.
      body: DrawCanvas(),
    );
  }
}
