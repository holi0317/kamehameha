import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:image_crop/image_crop.dart';
import 'package:meta/meta.dart';

const _API_TOKEN = 'I55p9nD5XhF5wng1yiDKhNXUL6aF4tHX352brV';

class ConvertException implements Exception {
  ConvertException(this.message);

  String message;

  @override
  String toString() {
    return message;
  }
}

/// Convert image by cropping the given image. Then convert to gcode and
/// thumbnail image.
///
/// Throws [ConvertException] with human-readable message when there is any
/// error occurred.
Future<ConvertRes> convertImage(File image, CropState crop) async {
  final scale = crop.scale;
  final area = crop.area;

  if (area == null) {
    throw ConvertException("Failed to get crop area");
  }

  final permissionsGranted = await ImageCrop.requestPermissions();
  if (!permissionsGranted) {
    throw ConvertException("Please grant permission for cropping image");
  }

  final croppedFile =
      await ImageCrop.cropImage(file: image, area: area, scale: scale);

  String buf;
  try {
    final dio = Dio();
    FormData formData =
        FormData.from({"image": UploadFileInfo(croppedFile, "img.png")});
    Response<String> response = await dio.post("https://jeffwong7.xyz",
        data: formData, options: Options(headers: {'X-API-TOKEN': _API_TOKEN}));
    buf = response.data;
  } on DioError catch (e) {
    print(e);
    throw ConvertException("Failed to convert image: ${e.message}");
  }

  Map userMap = jsonDecode(buf);
  final result = ConvertRes.fromJson(userMap);

  // TODO Save gcode and image in recent files

  return result;
}

class ConvertRes {
  final Uint8List image;
  final String gcode;

  ConvertRes({@required this.image, @required this.gcode});

  ConvertRes.fromJson(Map<String, dynamic> json)
      : image = Base64Decoder().convert(json['image']),
        gcode = json['gcode'];
}
