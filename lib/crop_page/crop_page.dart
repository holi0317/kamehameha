import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_crop/image_crop.dart';
import 'package:kamehameha/crop_page/convert_image.dart';

/// Display a crop image page for user to crop given image.
///
/// After the user has confirmed to crop the image to given size, this widget
/// will invoke cropping process and return the generated gcode from cropped image
/// to caller of this widget.
///
/// This widget expects itself to be rendered as a new page using [Navigator.push].
///
/// If return data (by using [Navigator.pop]) is null, that means user has cancelled
/// cropping process.
/// Otherwise, this will return [ConvertRes] as result.
///
/// If cropping process has error, this widget will stop the crop and allow user
/// to retry. Which means caller does not need to do error handling.
///
/// Given [image] must be in [File] format as this is required for image_crop
/// library to operate.
class CropPage extends StatefulWidget {
  CropPage({Key key, @required this.image}) : super(key: key);

  final File image;

  @override
  _CropPageState createState() => _CropPageState();
}

class _CropPageState extends State<CropPage> {
  final cropKey = GlobalKey<CropState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    final body = <Widget>[];

    body.add(Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Crop image"),
        leading: null,
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Crop(
                key: cropKey,
                image: FileImage(widget.image),
                aspectRatio: 1.0,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                RaisedButton(
                  child: const Text('Back'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  onPressed:
                      _loading ? null : () => Navigator.pop(context, null),
                ),
                RaisedButton(
                    child: const Text('Proceed'),
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    onPressed: _loading ? null : () => _convertImage(context))
              ],
            ),
          )
        ],
      ),
    ));

    if (_loading) {
      final modal = Stack(
        children: <Widget>[
          Opacity(
            opacity: 0.5,
            child: const ModalBarrier(dismissible: false, color: Colors.grey),
          ),
          Center(
            child: Dialog(
              child: Padding(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: CircularProgressIndicator(),
                    ),
                    Text("Processing image",
                        style: Theme.of(context).primaryTextTheme.subhead)
                  ],
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
              ),
            ),
          ),
        ],
      );
      body.add(modal);
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Stack(
        children: body,
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return !_loading;
  }

  Future<void> _convertImage(BuildContext context) async {
    setState(() {
      _loading = true;
    });

    try {
      final result = await convertImage(widget.image, cropKey.currentState);
      final popRes = Navigator.pop(context, result);
      if (!popRes) {
        scaffoldKey.currentState.showSnackBar(const SnackBar(
            content: const Text("Failed to pop route. This is a bug!")));
      }
    } on ConvertException catch (e) {
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(e.message)));
    } finally {
      setState(() {
        _loading = false;
      });
    }
  }
}
