import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class EngravePage extends StatefulWidget {
  EngravePage({Key key, @required this.gcode, this.image}) : super(key: key);

  /// Gcode to be engraved
  final String gcode;

  /// Image thumbnail for this gcode.
  /// If this is null, no thumbnail will be shown
  final ImageProvider image;

  @override
  _EngravePageState createState() => _EngravePageState();
}

List<String> splitGcode(String gcode) {
  List<String> res = [];
  final buffer = StringBuffer();

  for (var line in gcode.split("\n")) {
    if (line == "" && buffer.length == 0) {
      continue;
    }
    if (line == "" || buffer.length >= 500) {
      res.add(buffer.toString());
      buffer.clear();
    }
    if (line == "") {
      continue;
    }

    buffer.write(line);
    buffer.write("\r\n");
  }

  return res;
}

/// Create a stream emitting response to "?" command from all bluetooth messages.
Stream<String> makeQsStream(Stream<String> stream) async* {
  final buf = StringBuffer();

  await for (var msg in stream) {
    for (var i = 0; i < msg.length; i++) {
      final char = msg[i];
      if (char == ">") {
        buf.write(char);
        yield buf.toString();
        buf.clear();
      } else if (char == "<" || buf.length > 0) {
        buf.write(char);
      }
    }
  }
}

class _EngravePageState extends State<EngravePage> {
  /// Short hand for accessing FlutterBluetoothSerial in this class
  FlutterBluetoothSerial _bt = FlutterBluetoothSerial.instance;

  /// List of messages from bluetooth serial in this connection.
  ///
  /// Messages here would exclude message "o", "k" and "ok".
  /// Line breaks in messages would be removed from this list.
  /// This will get cleared when a new connection is initialized.
  /// But will not be cleared on disconnect.
  List<String> _text = [];

  /// Stream for messages from bluetooth serial.
  ///
  /// All line breaks are removed. And message with "o", "k" and "ok" are
  /// filtered
  Stream<String> _read$;

  /// Stream of responses for command "?".
  ///
  /// The response would be wrapped in '<' and '>' as a String.
  Stream<String> _question$;

  bool connected = false;
  bool uploading = false;

  @override
  void initState() {
    super.initState();

    _read$ = _bt
        .onRead()
        .map((msg) => msg.replaceAll(RegExp(r"\r|\n", multiLine: true), ""))
        .skipWhile((msg) => msg == "k" || msg == "o" || msg == "ok")
        .asBroadcastStream();

    _read$.listen((msg) => setState(() {
          //_text.add(msg);
        }));

    _question$ = makeQsStream(_read$).asBroadcastStream();

    _bt.onStateChanged().listen((_) async {
      final isConnected = await _bt.isConnected;
      setState(() {
        connected = isConnected;
      });
    });
    _bt.isConnected.then((con) => setState(() {
          connected = con;
        }));
  }

  Future<void> _connect() async {
    if (await _bt.isConnected) {
      // Must be disconnected state
      _snack("Cannot connect as it is connected");
      return;
    }
    List<BluetoothDevice> devices = await _bt.getBondedDevices();
    final matched = devices.where((el) => el.name == "HC-06");
    if (matched.length != 1) {
      // One and only one HC-06 is required
      _snack("Please pair with one and only one HC-06 device");
      return;
    }

    setState(() {
      _text.clear();
    });
    final device = matched.first;
    await _bt.connect(device);
    setState(() {
      connected = true;
    });
    print("Connected to ${device.name}");
  }

  Future<void> _disconnect() async {
    await _bt.disconnect();
    setState(() {
      connected = false;
    });
    print('disconnected');
  }

  Future<void> _uploadGcode() async {
    final connected = await _bt.isConnected;
    if (!connected) {
      _snack("Please connect before sending gcode");
      return;
    }

    final splitted = splitGcode(widget.gcode);

    setState(() {
      uploading = true;
    });

    try {
      for (var chunk in splitted) {
        await _bt.write(chunk);
        setState(() {
          _text.add(chunk);
        });
        await _waitIdle();
      }
    } catch (e) {
      _snack("Failed to upload code: ${e.toString()}");
      print(e);
    } finally {
      setState(() {
        uploading = false;
      });
    }
  }

  void _snack(String message) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
  }

  Future<void> _waitIdle() async {
    var failedCount = 0;

    while (failedCount < 5) {
      await _bt.write("?\r\n");
      final res = await _question$.first
          .timeout(const Duration(milliseconds: 500), onTimeout: () => null);
      if (res == null) {
        failedCount++;
      } else if (res.toLowerCase().contains("idle")) {
        break;
      } else {
        await Future.delayed(const Duration(milliseconds: 200));
      }
    }
  }

  Future<void> _showGcode() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Source gcode'),
          content: Container(
              color: Colors.grey,
              child: SingleChildScrollView(child: Text(widget.gcode))),
          actions: <Widget>[
            FlatButton(
              child: const Text("Copy to clipboard"),
              onPressed: () {
                Clipboard.setData(ClipboardData(text: widget.gcode));
              },
            ),
            FlatButton(
              child: const Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Engrave!"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              widget.image == null
                  ? null
                  : InkWell(
                      onTap: _showGcode,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image(image: widget.image),
                      ),
                    ),
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.center,
                  spacing: 12.0,
                  runSpacing: 4.0,
                  children: <Widget>[
                    connected
                        ? null
                        : RaisedButton(
                            onPressed: _connect,
                            child: const Text('Connect'),
                            color: Theme.of(context).accentColor,
                          ),
                    connected
                        ? RaisedButton(
                            onPressed: _disconnect,
                            child: const Text('Disconnect'))
                        : null,
                    connected
                        ? RaisedButton(
                            onPressed: uploading ? null : _uploadGcode,
                            child: const Text('Send G-code'),
                            color: Theme.of(context).accentColor)
                        : null,
                  ].where((w) => w != null).toList(),
                ),
              )
            ].where((w) => w != null).toList(),
          ),
          Expanded(child: MessagesList(messages: _text))
        ],
      ),
    );
  }
}

class MessagesList extends StatelessWidget {
  MessagesList({Key key, this.messages}) : super(key: key);

  final List<String> messages;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        itemBuilder: (BuildContext context, int index) => Text(messages[index]),
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemCount: messages.length);
  }
}
