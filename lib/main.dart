import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kamehameha/crop_page/convert_image.dart';
import 'package:kamehameha/crop_page/crop_page.dart';
import 'package:kamehameha/draw_page/draw_page.dart';
import 'package:kamehameha/engraving_page.dart';
import 'package:kamehameha/example_page.dart';
import 'package:kamehameha/manual_page.dart';

void main() => runApp(Root());

class Root extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Engraver controller',
        theme: ThemeData(
          primarySwatch: Colors.lightGreen,
        ),
        home: new HomePage());
  }
}

class HomePage extends StatelessWidget {
  Future<void> _sendImage(BuildContext context, ImageSource source) async {
    final image = await ImagePicker.pickImage(source: source);
    if (image == null) {
      return;
    }
    ConvertRes code = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => CropPage(image: image)));
    if (code == null) {
      return;
    }
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => EngravePage(
                gcode: code.gcode,
                image: MemoryImage(code.image),
              )),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Laser Engraver Controller')),
        body: Center(
          child: SingleChildScrollView(
              child: Wrap(
            spacing: 8.0,
            runSpacing: 4.0,
            children: <Widget>[
              HomePageBtn(
                icon: Icons.add_photo_alternate,
                title: Text("From gallery"),
                onPressed: () => _sendImage(context, ImageSource.gallery),
              ),
              HomePageBtn(
                icon: Icons.add_a_photo,
                title: Text("Take a photo"),
                onPressed: () => _sendImage(context, ImageSource.camera),
              ),
              HomePageBtn(
                  icon: Icons.color_lens,
                  title: Text("Draw"),
                  onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DrawPage()),
                      )),
              HomePageBtn(
                icon: Icons.insert_drive_file,
                title: Text("Recent files"),
              ),
              HomePageBtn(
                icon: Icons.lightbulb_outline,
                title: Text("Examples"),
                onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ExamplePage()),
                    ),
              ),
              HomePageBtn(
                icon: Icons.send,
                title: Text("G-code file"),
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ManualPage())),
              )
            ],
          )),
        ));
  }
}

class HomePageBtn extends StatelessWidget {
  HomePageBtn(
      {Key key, @required this.icon, @required this.title, this.onPressed})
      : super(key: key);

  final IconData icon;
  final Widget title;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              width: 110,
              height: 100,
              margin: EdgeInsets.all(20.0),
              child: RaisedButton(
                  onPressed: onPressed,
                  color: Colors.green,
                  padding: EdgeInsets.all(5.0),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[Icon(icon), title])))
        ]);
  }
}
