import "package:flutter/material.dart";
import 'package:flutter/services.dart';
import 'package:kamehameha/engraving_page.dart';

/// Represent a gcode that is stored in asset.
///
/// Paths here assumes the paths could be loaded using [rootBundle.loadString]
/// function.
class _ExampleCode {
  /// Path to the actual gcode
  final String codePath;

  /// Name of the engraved product
  final String name;

  /// Path to the thumbnail (in PNG format)
  /// Size must be 96x96 pixel
  final String thumbnail;

  _ExampleCode({@required this.codePath, @required this.name, this.thumbnail});
}

final _examples = [
  _ExampleCode(
      codePath: "assets/like.nc", name: "Like", thumbnail: "assets/like.png"),
  _ExampleCode(
      codePath: "assets/trebleclef.nc",
      name: "Treble Clef",
      thumbnail: "assets/trebleclef.png"),
  _ExampleCode(
      codePath: "assets/doraemon_f30v.nc",
      name: "Doraemon",
      thumbnail: "assets/doraemon.png"),
  _ExampleCode(
      codePath: "assets/pikachu.nc",
      name: "Pikachu",
      thumbnail: "assets/pikachu.png"),
];

class ExamplePage extends StatefulWidget {
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  var _loading = false;

  List<Widget> _buildActions() {
    if (!_loading) {
      return [];
    }
    return [CircularProgressIndicator()];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Engrave from examples"),
          actions: _buildActions(),
        ),
        body: ListView.builder(
            primary: true,
            itemCount: _examples.length,
            itemBuilder: (BuildContext context, int index) => ListTile(
                  enabled: !_loading,
                  onTap: () async {
                    setState(() {
                      _loading = true;
                    });
                    final item = _examples[index];
                    String code = "";

                    try {
                      code = await rootBundle.loadString(item.codePath);
                    } catch (e) {
                      Scaffold.of(context).showSnackBar(SnackBar(
                        content: Text("Failed to load example"),
                      ));
                      print(e);
                      return;
                    }

                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EngravePage(
                                gcode: code,
                                image: AssetImage(item.thumbnail),
                              )),
                    );
                    setState(() {
                      _loading = false;
                    });
                  },
                  leading: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Image.asset(_examples[index].thumbnail)),
                  title: Text(_examples[index].name),
                )));
  }
}
